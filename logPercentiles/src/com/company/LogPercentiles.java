package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogPercentiles {

    // example of filename pattern: 2018-13-10.log
    private static final SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
    private static final String LOG_DIR = "/var/log/httpd";
    private static final Logger LOGGER = Logger.getLogger("MyLog");

    // I could just use a SortedArray for my purpose, but here I will just implement it using a normal list in order to make my implementation not too dependent on existing java feature.
    // To do this, I need to maintain order for it each time I finish inserting new element to it.
    private static final List<Long> API_RESPONDING_TIMES = new ArrayList<>();

    static {
        // Setup logs
        try {
            FileHandler fh = new FileHandler("myLog");
            fh.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(fh);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args should be only one optional parameter passed in, which is the log dir. if not provided, use the default location
     */
    public static void main(String[] args) {

        if (args.length >= 2) {
            LOGGER.severe("Only the log dir could be passed in, which is defaultly '/var/log/httpd' if nothing gets passed in.");
            throw new IllegalArgumentException("Only the log dir could be passed in");
        }

        String logDir = args.length == 0 ? LOG_DIR : args[0];
        LOGGER.info("logDir=" + logDir);

        try (Stream<Path> walk = Files.walk(Paths.get(logDir))) {

            // get all logs files
            List<Path> logs = walk.filter(f -> Files.isRegularFile(f) && Utils.isLogFile(f.getFileName().toString(), DF)).collect(Collectors.toList());

            LOGGER.info("log files are: " + logs.stream().map(log -> log.getFileName().toString()).collect(Collectors.joining(",")));

            logs.forEach(log -> {
                try (Stream<String> stream = Files.lines(log)) {

                    // read each line of each file
                    stream.forEach(line -> {
                        try {
                            long responseTime = Utils.getResponseTime(line);
                            // if list is empty, no need to perform binary search
                            if (API_RESPONDING_TIMES.isEmpty()) {
                                API_RESPONDING_TIMES.add(responseTime);
                            } else {
                                // binary search
                                int index = Utils.binarySearchProperIndex(API_RESPONDING_TIMES, 0, API_RESPONDING_TIMES.size() - 1, responseTime);
                                API_RESPONDING_TIMES.add(index, responseTime);
                            }
                        } catch (Exception e) {
                            LOGGER.severe("file: " + log.toString() + " has an illegal line: " + line + ", this line is not processed.");
                        }
                        LOGGER.info("The current list is: " + API_RESPONDING_TIMES.stream().map(Object::toString).collect(Collectors.joining(",")));
                    });

                } catch (IOException e) {
                    LOGGER.severe("Error reading file: " + log.toString());
                    LOGGER.severe(e.getMessage());
                }
            });

        } catch (IOException e) {
            LOGGER.severe("Error listing files for: " + logDir);
            LOGGER.severe(e.getMessage());
        }

        // evaluate the log percentiles
        Utils.evaluate(API_RESPONDING_TIMES);
    }
}
