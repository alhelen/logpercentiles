package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;

class Utils {

    /**
     * Check if the current file is a log file
     * @param fileName of a specified log directory
     * @param df is a predefined date format that a log file is like
     * @return boolean that indicates if this file is a log file
     */
    static boolean isLogFile(String fileName, DateFormat df) {
        if (!fileName.endsWith(".log")) {
            return false;
        } else {
            try {
                df.parse(fileName.substring(0, fileName.length()-3));
                return true;
            } catch (ParseException pe) {
                // if ParseException is thrown, means this is not a log file
                return false;
            }
        }
    }

    /**
     * Get the response time of each line, which is each api call.
     * Assumptions:
     * 1. We only process the line with a 200 response, ignoring other status;
     * 2. We don't process the line with illegal format, like a line of an empty string, or the last element of the line could not be numerically parsed;
     * @param line
     * @return API response time extracted from the line
     */
    static long getResponseTime(String line) {
        String[] elements = line.split(" ");
        return Long.parseLong(elements[elements.length-1]);
    }

    /**
     * Get the correct index that the newItem should go to.
     * Again I actually could just use Collections.binarySearch(sortedList, newItem) instead, but ill still make my implementation not too rely on existing method.
     * @param sortedList a sortedList of the apiResponse times
     * @param start current starting searching index
     * @param newItem the item about to be added into the sortedList
     * @return the correct index that the newItem should go to
     */
    static int binarySearchProperIndex(List<Long> sortedList, int start, int end, long newItem) {

        // if start index >= end, means the interval is now too small so that start, (start + end) / 2, end are the same
        // in this case, terminate
        if (start >= end) {
            return sortedList.get(start) > newItem ? start : start + 1;
        }

        if (sortedList.get((start + end) / 2) == newItem) {
            return (start + end) / 2;
        } else if (sortedList.get((start + end) / 2) > newItem) {
            // if (start + end) / 2 > new item, search left
            return binarySearchProperIndex(sortedList, start, ((start + end) / 2) - 1, newItem);
        } else {
            // if (start + end) / 2 < new item, search right
            return binarySearchProperIndex(sortedList, ((start + end) / 2) + 1, end, newItem);
        }
    }

    /**
     * Print the evaluation results, i.e.
     * 90% of requests return a response in X ms
     * 95% of requests return a response in Y ms
     * 99% of requests return a response in Z ms
     * @param sortedList a sortedList of the apiResponse times
     */
    static void evaluate(List<Long> sortedList) {
        System.out.println("90% of requests return a response in " + sortedList.get(sortedList.size() * 90 / 100).toString() + " ms");
        System.out.println("95% of requests return a response in " + sortedList.get(sortedList.size() * 95 / 100).toString() + " ms");
        System.out.println("99% of requests return a response in " + sortedList.get(sortedList.size() * 99 / 100).toString() + " ms");
    }
}
