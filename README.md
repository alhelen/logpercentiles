# logPercentiles

My implementation is written in java8. The time complexity is O(nlogn), n is the number if total records/lines in all of the log files.
Because I traverse each line of all files, extract the api response time and store it in an ordered list.
Everytime I insert new response time to the list, I do a binary search to find its spot, which will take O(logn) for each, with n records/lines, will leads to a toal complexity of O(nlogn).
The space complexity will just be O(n) as I only store this many records in the list.

Some notes:
1. I am not using SortedList and stuff like Collections.binarySearch() to do it in order to be not too dependent on the language itself, I implemented it using a normal list and a self-defined binary search method;
2. I assume all log files should be in the format of YYYY-DD-MM.log, i dont process any files in other format. And I dont process any lines with a inappropriate format;
3. I add some logs in the code which increases the code lines, but if you would not mind, it helps me on debug